﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebas_graficos
{
    public class DatosGrafica
    {
        public const string CST_TIPO_BAR = "BAR";
        public const string CST_TIPO_COLUMNS = "COLUMNS";
        public const string CST_TIPO_PIE = "PIE";

        public string titulo { get; set; }
        public string tipo { get; set; }
        public List<SerieGrafica> series { get; set; }

        public DatosGrafica()
        {
            titulo = "";
            tipo = "";
            series = new List<SerieGrafica>();
        }
    }
}
