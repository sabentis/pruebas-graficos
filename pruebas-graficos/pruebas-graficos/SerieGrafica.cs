﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebas_graficos
{
    public class SerieGrafica
    {
        public string titulo { get; set; }
        public List<ValorGrafica> valores { get; set; }

        public SerieGrafica()
        {
            titulo = "";
            valores = new List<ValorGrafica>();
        }
    }
}
