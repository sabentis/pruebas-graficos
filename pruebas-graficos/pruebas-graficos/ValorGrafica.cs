﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebas_graficos
{
    public class ValorGrafica
    {
        public string nombre { get; set; }
        public decimal valor { get; set; }

        public ValorGrafica()
        {
            nombre = "";
            valor = 0;
        }
    }
}
