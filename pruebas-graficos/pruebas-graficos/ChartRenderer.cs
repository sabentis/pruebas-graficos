﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.DataVisualization.Charting;

namespace pruebas_graficos
{
    public class ChartRenderer
    {
        protected DatosGrafica data;

        public ChartRenderer(DatosGrafica data)
        {
            this.data = data;
        }

        public void render(string destination)
        {
            using (var ch = new Chart())
            {
                var cha = new ChartArea();

                ch.ChartAreas.Add(cha);

                var title = new Title(data.titulo);
                title.Font = new System.Drawing.Font("Verdana", (float) 20, System.Drawing.FontStyle.Bold);
                ch.Titles.Add(title);

                foreach (var s in data.series)
                {
                    Series serie = new Series(s.titulo);
                    serie.Legend = s.titulo;
                    serie.IsVisibleInLegend = true;

                    switch (this.data.tipo)
                    {
                        case DatosGrafica.CST_TIPO_BAR:
                            serie.ChartType = SeriesChartType.Bar;
                            break;
                        case DatosGrafica.CST_TIPO_PIE:
                            serie.ChartType = SeriesChartType.Pie;
                            break;
                        case DatosGrafica.CST_TIPO_COLUMNS:
                            serie.ChartType = SeriesChartType.Column;
                            break;
                        default:
                            throw new Exception("Chart type not supported.");

                    }

                    foreach (var p in s.valores)
                    {
                        DataPoint pnt = new DataPoint(0, (double)p.valor);
                        serie.Points.Add(pnt);
                    }

                    ch.Series.Add(serie);
                }

                ch.SaveImage(destination, ChartImageFormat.Jpeg);
            }
        }
    }
}
