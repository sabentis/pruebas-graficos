﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using pruebas_graficos;
using System.Windows.Forms.DataVisualization.Charting;

namespace pruebas_graficos_tests
{
    [TestClass]
    public class UnitTest1
    {

        /// <summary>
        /// Obtiene ejemplos de gráficas de tests.
        /// </summary>
        /// <returns></returns>
        protected List<DatosGrafica> getTestData()
        {

            var raw = new List<string>() {
                "{\"titulo\":\"Gráfico 1\",\"tipo\":\"\",\"series\":[{\"titulo\":\"NUM_INCAPACIDADES\",\"valores\":[{\"nombre\":\"Hombre\",\"valor\":13},{\"nombre\":\"Mujer\",\"valor\":20}]},{\"titulo\":\"NUM_DIAS\",\"valores\":[{\"nombre\":\"Hombre\",\"valor\":48},{\"nombre\":\"Mujer\",\"valor\":62}]}]}",
                "{\"titulo\":\"Gráfico 2\",\"tipo\":\"\",\"series\":[{\"titulo\":\"NUM_INCAPACIDADES\",\"valores\":[{\"nombre\":\"CENTRO TEST - 1981\",\"valor\":8},{\"nombre\":\"CENTRO TEST - 3711\",\"valor\":6},{\"nombre\":\"CENTRO TEST - 4594\",\"valor\":11},{\"nombre\":\"SEDE CENTRAL\",\"valor\":8}]},{\"titulo\":\"NUM_DIAS\",\"valores\":[{\"nombre\":\"CENTRO TEST - 1981\",\"valor\":23},{\"nombre\":\"CENTRO TEST - 3711\",\"valor\":47},{\"nombre\":\"CENTRO TEST - 4594\",\"valor\":22},{\"nombre\":\"SEDE CENTRAL\",\"valor\":18}]}]}",
                "{\"titulo\":\"Gráfico 3\",\"tipo\":\"\",\"series\":[{\"titulo\":\"NUM_INCAPACIDADES_A\",\"valores\":[{\"nombre\":\"Enero 2016/2015\",\"valor\":2},{\"nombre\":\"Febrero 2016/2015\",\"valor\":3},{\"nombre\":\"Marzo 2016/2015\",\"valor\":1},{\"nombre\":\"Abril 2016/2015\",\"valor\":3},{\"nombre\":\"Mayo 2016/2015\",\"valor\":6}]},{\"titulo\":\"NUM_DIAS_A\",\"valores\":[{\"nombre\":\"Enero 2016/2015\",\"valor\":18},{\"nombre\":\"Febrero 2016/2015\",\"valor\":26},{\"nombre\":\"Marzo 2016/2015\",\"valor\":7},{\"nombre\":\"Abril 2016/2015\",\"valor\":22},{\"nombre\":\"Mayo 2016/2015\",\"valor\":23}]},{\"titulo\":\"NUM_INCAPACIDADES_B\",\"valores\":[{\"nombre\":\"Enero 2016/2015\",\"valor\":2},{\"nombre\":\"Febrero 2016/2015\",\"valor\":3},{\"nombre\":\"Marzo 2016/2015\",\"valor\":3},{\"nombre\":\"Abril 2016/2015\",\"valor\":4},{\"nombre\":\"Mayo 2016/2015\",\"valor\":3}]},{\"titulo\":\"NUM_DIAS_B\",\"valores\":[{\"nombre\":\"Enero 2016/2015\",\"valor\":6},{\"nombre\":\"Febrero 2016/2015\",\"valor\":16},{\"nombre\":\"Marzo 2016/2015\",\"valor\":18},{\"nombre\":\"Abril 2016/2015\",\"valor\":15},{\"nombre\":\"Mayo 2016/2015\",\"valor\":4}]}]}"
            };

            List<DatosGrafica> result = new List<DatosGrafica>();

            foreach (var d in raw)
            {

                var grafica = Newtonsoft.Json.JsonConvert.DeserializeObject<DatosGrafica>(d);
                result.Add(grafica);
            }

            return result;
        }

        [TestMethod]
        public void TestDataWorks()
        {
            var samples = getTestData();

            // Para ejemplo generamos todos los tipos.
            List<string> types = new List<string>() { DatosGrafica.CST_TIPO_BAR, DatosGrafica.CST_TIPO_COLUMNS, DatosGrafica.CST_TIPO_PIE };

            var destination_dir = "d:\\graficos\\";
            if (!System.IO.Directory.Exists(destination_dir))
            {
                System.IO.Directory.CreateDirectory(destination_dir);
            }

            foreach (var sample in samples)
            {
                foreach (var type in types)
                {
                    sample.tipo = type;

                    var destination = destination_dir + MakeValidFileName("chart_" + sample.titulo + "_" + type + ".jpeg");

                    var renderer = new ChartRenderer(sample);

                    renderer.render(destination);
                }
            }
        }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }
    }
}
